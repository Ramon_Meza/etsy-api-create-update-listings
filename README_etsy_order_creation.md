
# Etsy Order Creation Script

## Overview
This script automates the creation of orders on Etsy using the Etsy API. It reads order data from an Excel file (`orders.xlsx`) and submits each row as an order to Etsy. The script respects Etsy's rate limits to ensure compliance with API usage policies.

## Prerequisites
- Python 3.x installed on your system.
- Required Python libraries: `requests` and `pandas`. These can be installed via pip if not already available:
  ```bash
  pip install requests pandas
  ```

## Etsy API Credentials
To use the Etsy API, you need the following credentials:
- API Key
- OAuth Token

Ensure you have these credentials before running the script.

## Excel File Structure
The Excel file (`orders.xlsx`) should have the following column headers:
- `shop_id`
- `listing_id`
- `quantity`
- `buyer_email`
- `buyer_first_name`
- `buyer_last_name`
- `shipping_address`
- `city`
- `state`
- `postal_code`
- `country`

Each row in the Excel file represents an order with the corresponding details.

### Sample Data
Here is an example of how the Excel file should be structured:

| shop_id | listing_id | quantity | buyer_email        | buyer_first_name | buyer_last_name | shipping_address     | city        | state  | postal_code | country |
|---------|------------|----------|--------------------|------------------|-----------------|----------------------|-------------|--------|-------------|---------|
| 1       | 1234567890 | 1        | testbuyer1@mail.com | Test             | Buyer1          | 123 Test St.         | Testville   | TV     | 12345       | US      |
| 1       | 1234567891 | 2        | testbuyer2@mail.com | Sample           | Buyer2          | 456 Sample Ave.      | Sampletown  | ST     | 67890       | US      |
| 1       | 1234567892 | 1        | testbuyer3@mail.com | Example          | Buyer3          | 789 Example Blvd.    | Examplecity | EX     | 13579       | US      |

## How the Program Works
1. **Load Dependencies**: The script imports the required libraries: `requests` for making API calls and `pandas` for reading the Excel file.
2. **Set Up API Credentials**: The script sets up the Etsy API credentials (API Key and OAuth Token) and prepares the request headers.
3. **Define Function to Create Order**:
   - The `create_order` function takes a row of order data and sends a POST request to the Etsy API to create an order.
   - The function constructs the payload from the row data and handles the API response.
4. **Read Data from Excel File**:
   - The script reads the order data from `orders.xlsx` into a pandas DataFrame.
5. **Process Each Row**:
   - For each row in the DataFrame, the script calls the `create_order` function to create a new order.
   - The script includes a delay (`time.sleep(1)`) between each API call to respect Etsy's rate limits.
6. **Completion Message**: Once all rows have been processed, the script prints a completion message.

## Instructions to Run the Script
1. **Set Up Etsy API Credentials**:
   - Replace `'your_api_key'` and `'your_oauth_token'` with your actual Etsy API Key and OAuth Token in the script.

2. **Prepare the Excel File**:
   - Create an Excel file named `orders.xlsx` with the required column headers and populate it with your order data.

3. **Run the Script**:
   - Save the script to a file, e.g., `create_etsy_orders.py`.
   - Run the script using Python:
     ```bash
     python create_etsy_orders.py
     ```

4. **Monitor Output**:
   - The script will print messages indicating the success or failure of each order creation. Monitor these messages to ensure that orders are created successfully.

5. **Handle Test Orders**:
   - Ensure your test listings are under $1 USD. After placing test orders, cancel them to ensure you are reimbursed and not charged the transaction fees.


