import requests
import pandas as pd
import time

# Etsy API credentials
API_KEY = 'api_key'
OAUTH_TOKEN = 'oauth_token'
HEADERS = {
    'x-api-key': API_KEY,
    'Authorization': f'Bearer {OAUTH_TOKEN}',
    'Content-Type': 'application/json'
}

# Function to create an order
def create_order(row):
    url = f'https://openapi.etsy.com/v3/application/shops/{row["shop_id"]}/orders'

    payload = {
        'listing_id': row['listing_id'],
        'quantity': row['quantity'],
        'buyer_email': row['buyer_email'],
        'buyer_first_name': row['buyer_first_name'],
        'buyer_last_name': row['buyer_last_name'],
        'shipping_address': {
            'line1': row['shipping_address'],
            'city': row['city'],
            'state': row['state'],
            'zip': row['postal_code'],
            'country': row['country']
        }
    }
    
    response = requests.post(url, headers=HEADERS, json=payload)
    
    if response.status_code == 201:
        print(f'Successfully created order for listing ID: {row["listing_id"]}')
    else:
        print(f'Failed to create order for listing ID: {row["listing_id"]}, Error: {response.json()}')
    
    return response

# Read data from Excel file
data = pd.read_excel('orders.xlsx')

# Process each row and create an order
for index, row in data.iterrows():
    create_order(row)
    time.sleep(1)  # Respect Etsy rate limits (1 request per second)

print('All orders processed.')
