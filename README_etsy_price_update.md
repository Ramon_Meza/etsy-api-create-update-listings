
# Etsy Listing Price Update Script

## Overview
This script automates the updating of prices for existing listings on Etsy using the Etsy API. It reads listing data from an Excel file (`change_price.xlsx`) and updates the price for each listing specified in the file. The script respects Etsy's rate limits to ensure compliance with API usage policies.

## Prerequisites
- Python 3.x installed on your system.
- Required Python libraries: `requests` and `pandas`. These can be installed via pip if not already available:
  ```bash
  pip install requests pandas
  ```

## Etsy API Credentials
To use the Etsy API, you need the following credentials:
- API Key
- OAuth Token

Ensure you have these credentials before running the script.

## Excel File Structure
The Excel file (`change_price.xlsx`) should have the following column headers:
- `shop_id`
- `listing_id`
- `price`

Each row in the Excel file represents a listing with the corresponding details to update its price.

### Sample Data
Here is an example of how the Excel file should be structured:

| shop_id | listing_id | price |
|---------|------------|-------|
| 1       | 101        | 19.99 |
| 1       | 102        | 29.99 |
| 1       | 103        | 39.99 |

## How the Program Works
1. **Load Dependencies**: The script imports the required libraries: `requests` for making API calls and `pandas` for reading the Excel file.
2. **Set Up API Credentials**: The script sets up the Etsy API credentials (API Key and OAuth Token) and prepares the request headers.
3. **Define Function to Update Listing Price**:
   - The `update_listing_price` function takes a row of listing data and sends a PATCH request to the Etsy API to update the listing price.
   - The function constructs the payload from the row data and handles the API response.
4. **Read Data from Excel File**:
   - The script reads the listing data from `change_price.xlsx` into a pandas DataFrame.
5. **Process Each Row**:
   - For each row in the DataFrame, the script calls the `update_listing_price` function to update the listing price.
   - The script includes a delay (`time.sleep(1)`) between each API call to respect Etsy's rate limits.
6. **Completion Message**: Once all rows have been processed, the script prints a completion message.

## Instructions to Run the Script
1. **Set Up Etsy API Credentials**:
   - Replace `'api_key_value'` and `'oauth_token_value'` with your actual Etsy API Key and OAuth Token in the script.

2. **Prepare the Excel File**:
   - Create an Excel file named `change_price.xlsx` with the required column headers and populate it with your listing data.

3. **Run the Script**:
   - Save the script to a file, e.g., `update_etsy_prices.py`.
   - Run the script using Python:
     ```bash
     python update_etsy_prices.py
     ```

4. **Monitor Output**:
   - The script will print messages indicating the success or failure of each price update. Monitor these messages to ensure that prices are updated successfully.
