import requests
import pandas as pd
import time

# Etsy API credentials
API_KEY = 'api_key_value'
OAUTH_TOKEN = 'oauth_token_value'
HEADERS = {
    'x-api-key': API_KEY,
    'Authorization': f'Bearer {OAUTH_TOKEN}',
    'Content-Type': 'application/x-www-form-urlencoded'
}

# Function to create a draft listing
def create_draft_listing(row):
    url = f'https://openapi.etsy.com/v3/application/shops/{row["shop_id"]}/listings'
    
    payload = {
        'title': row['title'],
        'description': row['description'],
        'quantity': row['quantity'],
        'price': row['price'],
        'taxonomy_id': row['taxonomy_id'],
        'shipping_profile_id': row['shipping_profile_id'],
        'return_policy_id': row['return_policy_id'],
        'tags': row['tags'],
        'materials': row['materials'],
        'who_made': row['who_made'],
        'when_made': row['when_made'],
        'item_weight': row['item_weight'],
        'item_weight_unit': row['item_weight_unit'],
        'item_length': row['item_length'],
        'item_width': row['item_width'],
        'item_height': row['item_height'],
        'item_dimensions_unit': row['item_dimensions_unit']
    }
    
    response = requests.post(url, headers=HEADERS, data=payload)
    
    if response.status_code == 201:
        print(f'Successfully created listing: {row["title"]}')
    else:
        print(f'Failed to create listing: {row["title"]}, Error: {response.json()}')
    
    return response

# Read data from Excel file
data = pd.read_excel('etsy_listing.xlsx')

# Process each row and create a draft listing
for index, row in data.iterrows():
    create_draft_listing(row)
    time.sleep(1)  # Respect Etsy rate limits (1 request per second)

print('All listings processed.')
