import requests
import pandas as pd
import time

# Etsy API credentials
API_KEY = 'api_key_value'
OAUTH_TOKEN = 'oauth_token_value'
HEADERS = {
    'x-api-key': API_KEY,
    'Authorization': f'Bearer {OAUTH_TOKEN}',
    'Content-Type': 'application/x-www-form-urlencoded'
}

# Function to update the price of a listing
def update_listing_price(row):
    url = f'https://openapi.etsy.com/v3/application/shops/{row["shop_id"]}/listings/{row["listing_id"]}'
    
    payload = {
        'price': row['price']
    }
    
    response = requests.patch(url, headers=HEADERS, data=payload)
    
    if response.status_code == 200:
        print(f'Successfully updated price for listing ID: {row["listing_id"]}')
    else:
        print(f'Failed to update price for listing ID: {row["listing_id"]}, Error: {response.json()}')
    
    return response

# Read data from Excel file
data = pd.read_excel('change_price.xlsx')

# Process each row and update the listing price
for index, row in data.iterrows():
    update_listing_price(row)
    time.sleep(1)  # Respect Etsy rate limits (1 request per second)

print('All listings processed.')
