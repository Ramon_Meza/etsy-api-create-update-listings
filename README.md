
# Etsy Listing Creation Script

## Overview
This script automates the creation of draft listings on Etsy using the Etsy API. It reads product data from an Excel file (`etsy_listing.xlsx`) and submits each row as a new draft listing on Etsy. The script respects Etsy's rate limits to ensure compliance with API usage policies.

## Prerequisites
- Python 3.x installed on your system.
- Required Python libraries: `requests` and `pandas`. These can be installed via pip if not already available:
  ```bash
  pip install requests pandas
  ```

## Etsy API Credentials
To use the Etsy API, you need the following credentials:
- API Key
- OAuth Token

Ensure you have these credentials before running the script.

## Excel File Structure
The Excel file (`etsy_listing.xlsx`) should have the following column headers:
- `shop_id`
- `title`
- `description`
- `quantity`
- `price`
- `taxonomy_id`
- `shipping_profile_id`
- `return_policy_id`
- `tags`
- `materials`
- `who_made`
- `when_made`
- `item_weight`
- `item_weight_unit`
- `item_length`
- `item_width`
- `item_height`
- `item_dimensions_unit`

Each row in the Excel file represents a product listing with the corresponding details.

### Sample Data
Here is an example of how the Excel file should be structured:

| shop_id | title                                      | description | quantity | price | taxonomy_id | shipping_profile_id | return_policy_id | tags               | materials       | who_made | when_made   | item_weight | item_weight_unit | item_length | item_width | item_height | item_dimensions_unit |
|---------|--------------------------------------------|-------------|----------|-------|-------------|---------------------|------------------|--------------------|-----------------|----------|-------------|-------------|------------------|-------------|------------|-------------|----------------------|
| 1       | Anchorage Alaska Souvenir Flask Black Unit | Description | 100      | 17.99 | 123         | 1                   | 1                | souvenir, flask    | stainless steel | i_did    | made_to_order | 1.0         | oz               | 10.0        | 5.0        | 2.0         | in                   |
| 1       | Anchorage Alaska Souvenir Flask Seafoam    | Description | 100      | 31.99 | 123         | 1                   | 1                | souvenir, flask    | stainless steel | i_did    | made_to_order | 1.0         | oz               | 12.0        | 6.0        | 3.0         | in                   |
| 1       | Anchorage Alaska Souvenir Flask Red        | Description | 100      | 54.99 | 123         | 1                   | 1                | souvenir, flask    | stainless steel | i_did    | made_to_order | 1.0         | oz               | 14.0        | 7.0        | 4.0         | in                   |

## How the Program Works
1. **Load Dependencies**: The script imports the required libraries: `requests` for making API calls and `pandas` for reading the Excel file.
2. **Set Up API Credentials**: The script sets up the Etsy API credentials (API Key and OAuth Token) and prepares the request headers.
3. **Define Function to Create Draft Listing**:
   - The `create_draft_listing` function takes a row of product data and sends a POST request to the Etsy API to create a draft listing.
   - The function constructs the payload from the row data and handles the API response.
4. **Read Data from Excel File**:
   - The script reads the product data from `etsy_listing.xlsx` into a pandas DataFrame.
5. **Process Each Row**:
   - For each row in the DataFrame, the script calls the `create_draft_listing` function to create a new draft listing.
   - The script includes a delay (`time.sleep(1)`) between each API call to respect Etsy's rate limits.
6. **Completion Message**: Once all rows have been processed, the script prints a completion message.

## Instructions to Run the Script
1. **Set Up Etsy API Credentials**:
   - Replace `'api_key_value'` and `'oauth_token_value'` with your actual Etsy API Key and OAuth Token in the script.

2. **Prepare the Excel File**:
   - Create an Excel file named `etsy_listing.xlsx` with the required column headers and populate it with your product data.

3. **Run the Script**:
   - Save the script to a file, e.g., `create_etsy_listings.py`.
   - Run the script using Python:
     ```bash
     python create_etsy_listings.py
     ```

4. **Monitor Output**:
   - The script will print messages indicating the success or failure of each listing creation. Monitor these messages to ensure that listings are created successfully.
